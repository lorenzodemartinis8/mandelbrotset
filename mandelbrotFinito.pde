int maxIteration = 500;
void setup(){
 size(500,500); 
}
void draw(){
  loadPixels();
  pixelDensity(1);
  mandelbrot();
  updatePixels();
}

void mandelbrot(){
  for (int x = 0; x < width; x++) {
    // Loop through every pixel row
    for (int y = 0; y < height; y++) {   
      //Interested ranges
      float a = map(x,0,width,-2,2);
      float b = map(y,0,height,-2,2);
      Complex c = new Complex(a,b);
      Complex z = new Complex(0,0);
      CalcComplex calc = new CalcComplex();
      int i;
      for(i = 0;i < maxIteration;i++) {
        z = calc.sum(calc.multi(z,z),c);
        if(Float.isNaN(z.real)){  
         break; 
        }
      }
      colorMode(HSB, 360, 100, 100);
      // Use the formula to find the 1D location
      int loc = (x + y * width);
       if(z.real <= 2){
         pixels[loc] = color(0,0,0);         
        }else{     
         pixels[loc] = color(i,i,i);
        }        
    }
  }
}
class Complex {
    float real;   // the real part
    float img;   // the imaginary part
    public Complex(float real, float img) {
        this.real = real;
        this.img = img;
    }
}

class CalcComplex{
    public  Complex multi(Complex b,Complex a) {
        float real = a.real * b.real - a.img * b.img;
        float img = a.real * b.img + a.img * b.real;
        return new Complex(real, img);
    }
    public  Complex sum(Complex c1,Complex a)
   {
  //creating a temporary complex number to hold the sum of two numbers
        Complex temp = new Complex(0, 0);

        temp.real = c1.real + a.real;
        temp.img = c1.img + a.img;
        
        //returning the output complex number
        return temp;
    }
}
